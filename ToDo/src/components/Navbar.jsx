import React from "react";
import {NavLink} from "react-router-dom";

function Navbar (){
  return (
   <nav className='navbar navbar-dark navbar-expand-lg bg-primary'>
     <ul className="navbar-nav">
       <li className="nav-item">
         <NavLink
          className="nav-link"
          to="/"
          exact
         >
           Home
         </NavLink>
       </li>
       <li className="nav-item">
         <NavLink
          className="nav-link"
          to="/about"
          exact
         >
           About
         </NavLink>
       </li>
     </ul>
   </nav>
  )
}

export default Navbar