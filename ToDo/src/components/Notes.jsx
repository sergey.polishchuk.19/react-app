import React from "react";

function Notes ({notes}){
  return(
   <ul className='list-group'>
     {notes.map(note => (
     <li key={note.id} className='list-group-item'>{note.title}</li>
     ))}
   </ul>
  )
}

export default Notes