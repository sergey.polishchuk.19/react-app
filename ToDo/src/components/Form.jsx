import React from "react";

function Form (){
  return(
   <form>
     <div className="from-group">
       <input
        type="text"
        className="from-control"
        placeholder='Введите название заметки'
       />
     </div>
   </form>
  )
}

export default Form